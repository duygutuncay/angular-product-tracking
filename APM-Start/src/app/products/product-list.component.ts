import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription, throwError } from "rxjs";
import { IProduct } from './product';
import { ProductService } from './product.service'

@Component({
    selector: 'pm-products',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit, OnDestroy {
    pageTitle: string = 'Product List';
    imageWidth: number = 50;
    imageMargin: number = 2;
    showImage : boolean = false;
    filteredProducts: IProduct[] = [];
    products: IProduct[] = [];
    errorMessage: string = ''
    // ! means I'll deal with the type of it later also means: sub | undefined
    sub!: Subscription;

    private _listFilter: string = '';
    get listFilter(): string {
        return this._listFilter;
    }
    set listFilter(value:string) {
        this._listFilter = value;
        console.log('In setter:', value);
        this.filteredProducts = this.performFilter(value);
    }

    constructor(private productService: ProductService) {

    }
    ngOnInit(): void {
        // this.products = this.productService.getProducts();
        // subscribe ile get requesti baslatmis oluyoruz
        this.sub = this.productService.getProducts().subscribe({
            next: products => {
                this.products = products;
                this.filteredProducts = this.products;
            },
            error: err => this.errorMessage = err
        });
        
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    toggleImage(): void {
        this.showImage = !this.showImage
    }

    performFilter(filterBy: string): IProduct[] {
        filterBy = filterBy.toLocaleLowerCase();
        return this.products.filter((product: IProduct) => product.productName.toLocaleLowerCase().includes(filterBy));
    }
    onRatingClicked(message: string): void {
        this.pageTitle = 'Product List: ' + message
    }
}